﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WarmupClass : MonoBehaviour {

	/// Tag to verify is the right move was performed
	[SerializeField]
	string strTag;

	/// Function that allows to detect eventual collisions
	public void OnCollisionEnter(Collision Collision){ 	

		/// If the collision was the expected one, the system enters WaitForSec function
		if (Collision.collider.tag == strTag) {
			StartCoroutine ("WaitForSec");
		}
	}
		
	public IEnumerator WaitForSec()
	{
		/// The system waits 2 seconds
		yield return new WaitForSeconds(2);
		/// The system loads the next scene
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

}
