﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SubGame3 : MonoBehaviour {


	/// collision trigger
	[SerializeField]
	string strTag_aux;

	/// collision trigger
	[SerializeField]
	string strTag;

	/// collision trigger
	[SerializeField]
	string strTag2;

	/// collision trigger
	[SerializeField]
	string strTag3;

	/// collision trigger
	[SerializeField]
	string strTag4;

	/// integer number responsible for storing score value before the subgame begins
	int aux_score;

	/// initial score value is store before the game begins
	void Start () {
		aux_score = SeriousGame.score;
	}

	/// Function that allows to detect eventual collisions
	public void OnCollisionEnter(Collision Collision)
	{ 	
		/// If the collision was the expected one, the system waits 1 second and destroys the colliding object
		if ( (Collision.collider.tag == strTag) )
		{
			StartCoroutine(waitfunction());
			Destroy (Collision.gameObject);
		}

		/// If the collision was the expected one, user's score is increased
		else if ( (Collision.collider.tag == strTag || Collision.collider.tag == strTag2 ) )
		{
			SeriousGame.score++;
		}

		/// If the collision was the expected one, the system waits 1 second and destroys the colliding object
		else if (Collision.collider.tag == strTag3 || Collision.collider.tag == strTag4) {
			StartCoroutine(waitfunction());
			Destroy (Collision.gameObject);
		}
	}
		

	void Update () {

		/// if the total score obtained in this subgame is bigger than 8 it means all cylinders were taken down and loads next scene
		if ( SeriousGame.score - aux_score > 8 )
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	/// waits one second before destroying the desired object
	public IEnumerator waitfunction()
	{
		yield return new WaitForSeconds(1);
	}
}
