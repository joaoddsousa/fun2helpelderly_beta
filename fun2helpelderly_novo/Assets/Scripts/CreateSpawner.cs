﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateSpawner : MonoBehaviour {


	/// Tag to the define the desired object
	[SerializeField]
	GameObject goCreate1;

	/// Tag to the define the desired object
	[SerializeField]
	GameObject goCreate2;

	/// Tag to the define the desired object
	[SerializeField]
	GameObject goCreate3;

	/// Tag to the define the desired object
	[SerializeField]
	GameObject goCreate4;

	/// Tag to the define the desired object
	[SerializeField]
	GameObject goCreate5;

	/// auxiliar game object to fill the unity  "Instantiate" function's arguments
	GameObject goCreateAux;

	/// integer for the random generation algorithm
	int decideObject;

	/// Tag to the define the desired spawn times
	[SerializeField]
	float fTimeIntervals;

	/// Tag to the define the desired spawn coordinates
	[SerializeField]
	Vector3 v3SpawnPosJitter;

	float fTimer = 0;

	/// Function to start to initialize the timer variable
	void Start () {
		fTimer = fTimeIntervals;
	}


	/// Function to update the timer variable and spawn the desired objects according to the "Random" algorithm
	void Update () {
		fTimer -= Time.deltaTime;

		if (fTimer <= 0) 
		{
			fTimer = fTimeIntervals;

			/// Each random generated number will equal to a different object to be created
			decideObject = Random.Range(1,6);

			if (decideObject == 1)
				goCreateAux = goCreate1;

			if (decideObject == 2)
				goCreateAux = goCreate2;

			if (decideObject == 3)
				goCreateAux = goCreate3;

			if (decideObject == 4)
				goCreateAux = goCreate4;

			if (decideObject == 5)
				goCreateAux = goCreate5;

			/// Unity UI function responsible for spawning the desired objects
			Instantiate (goCreateAux, transform.position, Quaternion.identity);

		}
	}
}
