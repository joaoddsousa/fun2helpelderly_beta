﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
///  all functions in this class are associated to GUI buttons
/// </summary>
public class MainMenu : MonoBehaviour {


	/// fuction to initialize the serious game first scene
	public void PlayGame()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
	}
		
	/// fuction to go back to the main menu
	public void BackToMenu()
	{
		SceneManager.LoadScene(0);
	}
		

	/// fuction to quit the serious game main menu
	public void QuitGame()
	{
		Debug.Log ("Quit!");
		Application.Quit ();
	}

}
