﻿using UnityEngine;

public class DesactivaVoltarMenu : MonoBehaviour
{
	/// This function hides or shows the "voltar ao menu" button in every game scene
	void Start()
    {
		/// If this flag is activated, the "voltar ao menu" button is not display anymore
        if (!ToggleScriptDesactivaActiva.activado)
            gameObject.SetActive(false); 
    }






}
