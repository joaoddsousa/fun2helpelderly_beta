﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class volumeChangerScript : MonoBehaviour
{

	/// Tag to pass the desired slider to control volume
	[SerializeField] private Slider SliderHandleValue;

	/// Tag to pass the audio source to be modified
	[SerializeField] private AudioSource musicVolumeController;

	/// Function to initialize the existing functions with the current audio values
	private void OnEnable()
	{
		SliderHandleValue.value = musicVolumeController.volume;
		StartCoroutine(MyUpdate());
	}

	/// Function to attribute the final audio source volume
	private void OnDisable()
	{
		StopCoroutine(MyUpdate());
	}

	/// Function to attribute the audio source volume correspondent to slide floating values
	IEnumerator MyUpdate()
	{
		musicVolumeController.volume = SliderHandleValue.value;

		/// Update the system every "0.033" seconds
		yield return new WaitForSeconds(0.033f);

		StartCoroutine(MyUpdate());
	}

}