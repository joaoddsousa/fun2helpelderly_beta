﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimerFunction : MonoBehaviour {

	public GameObject textDisplay;
	public int secondsLeft = 70;
	public bool takingAway = false;

	/// Function responsible for displaying the current time on the screen
	void Start () {
		textDisplay.GetComponent<Text> ().text = "00:" + secondsLeft;
	}
	
	/// Function responsible for verifying if the actual time remains bigger than 0 
	void Update () {
		if (takingAway == false && secondsLeft > 0)
		{
			StartCoroutine (TimerTake ());
		}
	}
		
	/// Function responsible for changing the game scene if the current time reaches 0
	IEnumerator TimerTake()
	{
		takingAway = true;
		yield return new WaitForSeconds(1);
		secondsLeft -= 1;

		/// If clause responsible for adjusting time text display
		if (secondsLeft < 10) {
			textDisplay.GetComponent<Text> ().text = "00:0" + secondsLeft;
				
		} else {
			textDisplay.GetComponent<Text> ().text = "00:" + secondsLeft;
		}

		/// Function responsible for changing the game scene when the time reaches the limit
		if (secondsLeft <= 0) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		}

		/// Goes back to the "Update()" function...
		takingAway = false;
	}
}
