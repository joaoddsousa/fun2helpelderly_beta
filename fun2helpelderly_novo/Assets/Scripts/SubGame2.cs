﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SubGame2 : MonoBehaviour {

	/// game objects to show the right color
	public GameObject textDisplay;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	string bola_branca;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	string bola_roxa;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	string bola_preta;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	string bola_verde;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	string bola_azul;

	/// Tag to define the color changing intervals
	[SerializeField]
	string bola_vermelha;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	float fTimeIntervals;

	/// float number to control the color string changing process
	float fTimer = 0;

	/// integer number to be randomly sorted to change the colors
	int color = 0;

	public string stringAux;

	/// Initialization of the time interval and string responsible for showing the right color on the screen
	void Start () {
		fTimer = fTimeIntervals;
		textDisplay.GetComponent<Text> ().text = "Cor: " + stringAux;
	}

	/// Function that allows to detect eventual collisions
	public void OnCollisionEnter(Collision Collision)
	{ 	
		/// If the collision was the expected one, user's score is increased. If it was not the expected one, user's score is decreased
		if ((Collision.collider.tag == bola_branca && color == 1) || (Collision.collider.tag == bola_roxa && color == 2) || (Collision.collider.tag == bola_preta && color == 3) || (Collision.collider.tag == bola_verde && color == 4) || (Collision.collider.tag == bola_azul && color == 5) || (Collision.collider.tag == bola_vermelha && color == 6)) {
			SeriousGame.score++;
		} else {
			SeriousGame.score--;
		}

	}

	void Update () {

		fTimer -= Time.deltaTime;

		/// Random color genarator algorithm
		if (fTimer <= 0) 
		{
			fTimer = fTimeIntervals;

			/// Each random generated number will equal to a different color printed on the screen
			color = Random.Range(1,7);

			if (color == 1)
				stringAux = "branco";

			if (color == 2)
				stringAux = "roxo";

			if (color == 3)
				stringAux = "preto";

			if (color == 4)
				stringAux = "verde";

			if (color == 5)
				stringAux = "azul";

			if (color == 6)
				stringAux = "vermelho";

			Debug.Log ("Cor: " + stringAux);
			// Display the color on the screen
			textDisplay.GetComponent<Text> ().text = "Cor: " + stringAux;
		}
	}
}
