﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class SubmitButton : MonoBehaviour {

	string name;

	/// Tag to pass the button name
	[SerializeField] private InputField aux_name;

	/// Tag to pass the introduced string
	private string _path;


	/// Function to submit the introduced text through the button press
	public void SubmitName(){

		name = aux_name.text;
		_path = CreateText();
	}
		

	/// Function to write the name and score parameters in the text file responsible for storing all the names and respective scores
	private string CreateText()
	{
		string path = Application.dataPath + "/Scores.txt";

		/// Creating the file or writting inside the existing file
		if (!File.Exists(path))
		{
			File.WriteAllText(path, name + "\n" + CorrectMove.score);
		}
			
		StreamWriter w = File.AppendText(path);
		w.WriteLine(name + "\n" + CorrectMove.score);
		w.Close();
		return path;
	}
}
