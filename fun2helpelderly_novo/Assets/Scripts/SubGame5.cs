﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SubGame5 : MonoBehaviour {


	/// Tag to verify is the right move was performed
	[SerializeField]
	string tag_right;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	string tag_wrong;

	/// game objects to show confirmation messages
	public GameObject uiObject_right;
	public GameObject uiObject_wrong;

	/// hides confirmation messages
	void start()
	{
		uiObject_right.SetActive (false);
		uiObject_wrong.SetActive (false);
	}

	/// Function that allows to detect eventual collisions
	public void OnCollisionEnter(Collision Collision){ 	


		/// If the collision was the expected one, the system destoys the colliding object, display a confirmation message, enters WaitForSec function and increases score
		if (Collision.collider.tag == tag_right) {
			Destroy (Collision.gameObject);
			uiObject_right.SetActive (true);
			StartCoroutine ("WaitForSec_right");
			SeriousGame.score++;
			Debug.Log ("Objeto certo! Score: " + SeriousGame.score.ToString ());
		} 

		/// If the collision was the expected one, the system destoys the colliding object, display a confirmation message, enters WaitForSec function and increases score
		else if (Collision.collider.tag == tag_wrong) {
			Destroy (Collision.gameObject);
			SeriousGame.score--;
			Debug.Log ("Objeto errado! Score: " + SeriousGame.score.ToString ());
			uiObject_wrong.SetActive (true);
			StartCoroutine ("WaitForSec_wrong");
		}
	}

	/// show confirmation message for 1 second
	IEnumerator WaitForSec_right()
	{
		yield return new WaitForSeconds (1);
		uiObject_right.SetActive (false);
	}

	/// show confirmation message for 1 second
	IEnumerator WaitForSec_wrong()
	{
		yield return new WaitForSeconds (1);
		uiObject_wrong.SetActive (false);
	}
}
