﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleScriptDesactivaActiva : MonoBehaviour
{
    public static bool activado;
    public Toggle desactivaMenuToggle;
    
	/// Initialization of the flag "activado" to display the "voltar ao menu" button
	void Start()
	{
		activado = desactivaMenuToggle.isOn;
	}

	/// Function that allows to change the flag "activado"
	public void ChangeValue()
    {
        activado = desactivaMenuToggle.isOn;
    }
}
