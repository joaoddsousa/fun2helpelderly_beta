﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ShowHighScore : MonoBehaviour {

	[SerializeField] private Text text_aux;

	public string[] txtNamesNscore;
	public List<string> names = new List<string>();
	public List<string> scores = new List<string>();


	/// Function that calls the function responsible for initializing the required variables, reading from the .txt file and displaying the latest scores in the respective sub-menu
	private void OnEnable()
	{
		FillListas();
		ReadAndShowScore();
		ShowTopFive();
	}

	/// Function that reads the score and name parameters from the .txt file
	private void ReadAndShowScore()
	{
		string path = Application.dataPath + "/Scores.txt";

		if (!File.Exists(path)) // n existe path ainda
			return;

		else
		{
			int numeroLinhas = File.ReadAllLines(path).Length;

			Debug.Log (numeroLinhas);

			//StreamReader _read = File.OpenText(path);
			txtNamesNscore = File.ReadAllLines(path);
			int j = 0;
			int k = 0;

			/// If the .txt file contains more than 5 names and respective scores...
			int indexinicial = numeroLinhas - 10;

			if (indexinicial < 0) {
				indexinicial = 0;
			}

			/// Fill the .txt file with the desired information
			for (int i = indexinicial; i < numeroLinhas; i++)	
			{

				if (i % 2 == 0) {
					
					names [k] = txtNamesNscore [i];
					k++;
				} else {
					scores [j] = txtNamesNscore [i];
					j++;
				}
			}

		}

	}

	/// Function that stores the desired information and displays the most recent five score in the "Latest Scores" sub-menu
	private void ShowTopFive()
	{
		text_aux.text = names[4] + " - " + scores[4] + "\n" +
			names[3] + " - " + scores[3] + "\n" +
			names[2] + " - " + scores[2] + "\n" +
			names[1] + " - " + scores[1] + "\n" +
			names[0] + " - " + scores[0];
	}

	/// Function that initializes the score and name parameters
	private void FillListas()
	{
		for(int i = 0; i < 5; i++)
		{
			names.Add("");
			scores.Add("");
		}
	}
		
}
