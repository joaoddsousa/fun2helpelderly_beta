﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SeriousGame : MonoBehaviour {

	/// Serious game score public variable
	public static int score;

	/// Serious game time public variable
	public static int time;

}
