﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SubGame4 : MonoBehaviour {

	/// Tag to verify is the right move was performed
	[SerializeField]
	string tag_right;

	/// Tag to verify is the wrong move was performed
	[SerializeField]
	string tag_wrong;

	/// Sets the timer to 0 to control new scene loading timming
	float fTimer = 0;

	/// Tag to verify is the wrong move was performed
	bool nextlvl = false;

	/// game objects to show confirmation messages
	public GameObject uiObject_right;
	public GameObject uiObject_wrong;

	/// hides confirmation messages initially
	void start()
	{
		uiObject_right.SetActive (false);
		uiObject_wrong.SetActive (false);
	}

	/// If the collision was the expected one, the system displays a confirmation message, activates "nextlvl" flag, enters WaitForSec function and increases score
	void OnCollisionEnter(Collision Collision)
	{ 	
		if (Collision.collider.tag == tag_right) 
		{
			uiObject_right.SetActive (true);
			nextlvl = true;
			StartCoroutine ("WaitForSec");
			SeriousGame.score++;
		} 	

		/// If the collision was not the expected one, the system displays a confirmation message, activates "nextlvl" flag, enters WaitForSec function and decreases score
		else if (Collision.collider.tag == tag_wrong) 
		{
			uiObject_wrong.SetActive (true);
			nextlvl = true;
			StartCoroutine ("WaitForSec_wrong");
			SeriousGame.score--;
		}
	}

	/// show confirmation message for 1 second
	IEnumerator WaitForSec_right()
	{
		yield return new WaitForSeconds (1);
		uiObject_right.SetActive (false);
	}

	/// show confirmation message for 1 second
	IEnumerator WaitForSec_wrong()
	{
		yield return new WaitForSeconds (1);
		uiObject_wrong.SetActive (false);
	}

	/// constantly confirmates if the "nextlvl" is activated. 
	void Update () {

			if (nextlvl == true)
			{
				fTimer -= Time.deltaTime;
			/// if it is, the system wais 3 seconds and then loads the next scene	
			if (fTimer <= -3 && nextlvl == true) {
					SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
				}
			}
	}
		
}

