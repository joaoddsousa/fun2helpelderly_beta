﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Warmup : MonoBehaviour {

	public static int score;

	/// Criação de uma tag para associar ao toque da mão
	[SerializeField]
	string strTag;

	[SerializeField]
	bool bDestroyOther = false;

	/// Função que permite a alteração da pergunta assim que uma resposta é selecionada
	public void OnCollisionEnter(Collision Collision){ 	

		/// Se tag seja for a desejada, ocorre uma mudança de cena
		if (Collision.collider.tag == strTag && bDestroyOther) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
		}
			
	}
}
