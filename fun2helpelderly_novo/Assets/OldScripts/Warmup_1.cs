﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Warmup_1 : MonoBehaviour {

	public static int score;

	/// Criação de uma tag para associar ao toque da mão
	[SerializeField]
	string strTag;

	/// Função que permite a alteração da pergunta assim que uma resposta é selecionada
	public void OnCollisionEnter(Collision Collision){ 	

		/// Se tag seja for a desejada, ocorre uma mudança de cena
		if (Collision.collider.tag == strTag) {
			StartCoroutine ("WaitForSec");
		}
			
	}


	public IEnumerator WaitForSec()
	{
		yield return new WaitForSeconds(2);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}
}
