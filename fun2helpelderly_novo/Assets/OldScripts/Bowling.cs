﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bowling : MonoBehaviour {

	//public static int score;

	/// Criação de uma tag para associar ao toque da mão
	[SerializeField]
	string strTag;

	[SerializeField]
	string strTag2;

	[SerializeField]
	string strTag3;

	[SerializeField]
	string strTag4;

	/// Função que permite a alteração da pergunta assim que uma resposta é selecionada
	public void OnCollisionEnter(Collision Collision)
	{ 	
		/// Se tag seja for a desejada, ocorre uma mudança de cena
		if ( (Collision.collider.tag == strTag || Collision.collider.tag == strTag2 ) )
		{
			
			Debug.Log ("Acertou no objeto! Score: " + CorrectMove.score.ToString ());
			//Collision.collider.tag = "Untagged";

			//CorrectMove.score++;
			//StartCoroutine(waitfunction());
			//Destroy (this.gameObject);

		}

		if (Collision.collider.tag == strTag3 || Collision.collider.tag == strTag4) {
			//Destroy (this.gameObject);
			//Collision.collider.tag = "Untagged";
			StartCoroutine(waitfunction());
		}
	}

	public IEnumerator waitfunction()
	{
		//CorrectMove.score++;

		yield return new WaitForSeconds(2);
		CorrectMove.score++;
		Destroy (this.gameObject);
	}
	// SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);


}
