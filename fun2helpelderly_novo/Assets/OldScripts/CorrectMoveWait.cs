﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CorrectMoveWait : MonoBehaviour {

	[SerializeField]
	string strTag;

	public GameObject uiObject;

	void start()
	{
		uiObject.SetActive (false);
	}

	public void OnCollisionEnter (Collision Collision)
	{
		if (Collision.collider.tag == strTag) {
			uiObject.SetActive (true);
			StartCoroutine ("WaitForSec");
		}
	}

	IEnumerator WaitForSec()
	{
		yield return new WaitForSeconds (1);
		uiObject.SetActive (false);
		//Destroy (uiObject);
		//Destroy (gameObject);
	}

}
