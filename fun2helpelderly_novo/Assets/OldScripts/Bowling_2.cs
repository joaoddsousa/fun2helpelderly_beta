﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bowling_2 : MonoBehaviour {

	int aux_score;

	void Start () {
		aux_score = CorrectMove.score;
		Debug.Log ("Pontuacao comparacao: " + aux_score.ToString ());
	}

	void Update () {
	
		if ( CorrectMove.score - aux_score > 8 )
			SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

}
