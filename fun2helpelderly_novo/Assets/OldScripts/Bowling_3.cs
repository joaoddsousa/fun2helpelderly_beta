﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Bowling_3 : MonoBehaviour {

	[SerializeField]
	string strTag;

	public void OnCollisionEnter(Collision Collision)
	{ 	
		/// Se tag seja for a desejada, ocorre uma mudança de cena
		if ((Collision.collider.tag == strTag)) {
			StartCoroutine (waitfunction ());
		}
	}

	public IEnumerator waitfunction()
	{
		yield return new WaitForSeconds(2);
		Destroy (this.gameObject);
	}

}
	
