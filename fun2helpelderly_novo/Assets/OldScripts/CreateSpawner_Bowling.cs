﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateSpawner_Bowling : MonoBehaviour {


	[SerializeField]
	GameObject goCreate1;

	[SerializeField]
	float fTimeIntervals;

	[SerializeField]
	Vector3 v3SpawnPosJitter;

	float fTimer = 0;

	void Start () {
		fTimer = fTimeIntervals;
	}

	void Update () {
		fTimer -= Time.deltaTime;

		if (fTimer < 25 && fTimer <= 0) 
		{
			fTimer = fTimeIntervals;
			Instantiate (goCreate1, transform.position, Quaternion.identity);
		}
	}
}
