﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColorSelector_NotUsed : MonoBehaviour {

	public GameObject textDisplay;

	[SerializeField]
	string bola_branca;

	[SerializeField]
	string bola_azulbebe;

	[SerializeField]
	string bola_preta;

	[SerializeField]
	string bola_verde;

	[SerializeField]
	string bola_azul;

	[SerializeField]
	string bola_vermelha;

	[SerializeField]
	float fTimeIntervals;

	int color = 0;

	string stringAux;

	float fTimer = 0;

	// Use this for initialization
	void Start () {
		textDisplay.GetComponent<Text> ().text = "Cor: " + stringAux;
	}

	void Update () {

		fTimer -= Time.deltaTime;

		if (fTimer <= 0) 
		{
			fTimer = fTimeIntervals;

			color = Random.Range(1,7);

			if (color == 1)
				stringAux = "branco";

			if (color == 2)
				stringAux = "azul_bebe";

			if (color == 3)
				stringAux = "preto";

			if (color == 4)
				stringAux = "verde";

			if (color == 5)
				stringAux = "azul";

			if (color == 6)
				stringAux = "vermelho";

			//Debug.Log ("Cor: " + stringAux);
		}
	}
}
