﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreFunction_g3 : MonoBehaviour {

	public GameObject textDisplay;

	// Use this for initialization
	void Start () {
		textDisplay.GetComponent<Text> ().text = "Score: " + CorrectMove.score;
	}
	
	// Update is called once per frame
	void Update () {
		textDisplay.GetComponent<Text> ().text = "Score: " + CorrectMove.score;
	}

}
