﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BowlingTiras : MonoBehaviour {

	//public static int score;

	/// Criação de uma tag para associar ao toque da mão
	[SerializeField]
	string strTag;

	/// Função que permite a alteração da pergunta assim que uma resposta é selecionada
	public void OnCollisionEnter(Collision Collision)
	{ 	
		/// Se tag seja for a desejada, ocorre uma mudança de cena
		if ( (Collision.collider.tag == strTag) )
		{
			StartCoroutine(waitfunction());
			//Destroy (this.gameObject);
		}
	}

	public IEnumerator waitfunction()
	{
		yield return new WaitForSeconds(1);
		Destroy (this.gameObject);
	}
			
}
