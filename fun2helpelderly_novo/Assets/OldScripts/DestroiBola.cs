﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DestroiBola : MonoBehaviour {

	public static int score;

	/// Criação de uma tag para associar ao toque da mão
	[SerializeField]
	string strTag;


	/// Função que permite a alteração da pergunta assim que uma resposta é selecionada
	public void OnCollisionEnter(Collision Collision){ 	

		/// Se tag seja for a desejada, ocorre uma mudança de cena
		if (Collision.collider.tag == strTag) {
			Destroy (Collision.gameObject);
		}
	
	}

}