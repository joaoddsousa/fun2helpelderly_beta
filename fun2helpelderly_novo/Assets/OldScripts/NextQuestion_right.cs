﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextQuestion_right : MonoBehaviour {

	/// Criação de uma tag para associar ao toque da mão
	[SerializeField]
	string strTag;

	float fTimer = 0;

	bool nextlvl = false;

	public GameObject uiObject;

	void start()
	{
		uiObject.SetActive (false);
	}

	void OnCollisionEnter(Collision Collision)
	{ 	
		if (Collision.collider.tag == strTag) 
		{
			uiObject.SetActive (true);

			nextlvl = true;

			StartCoroutine ("WaitForSec");
			Debug.Log("Resposta certa!");
			CorrectMove.score++;
			//Destroy (this.gameObject);

			Debug.Log("Proxima pergunta em 3 segundos");
			//SceneManager.LoadScene(SceneManager.GetActiveScene ().buildIndex + 1);
		}
	}

	IEnumerator WaitForSec()
	{
		yield return new WaitForSeconds (1);
		//Destroy (uiObject);
		uiObject.SetActive (false);
		//Destroy (gameObject);
	}

	void Update () {

		if (nextlvl == true)
		{
			
			fTimer -= Time.deltaTime;

			if (fTimer <= -3 && nextlvl == true) {
				SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
			}
		}
	}
		
}
