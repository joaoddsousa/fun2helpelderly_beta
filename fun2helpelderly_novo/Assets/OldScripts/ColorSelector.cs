﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ColorSelector : MonoBehaviour {

	public GameObject textDisplay;

	[SerializeField]
	string bola_branca;

	[SerializeField]
	string bola_roxa;

	[SerializeField]
	string bola_preta;

	[SerializeField]
	string bola_verde;

	[SerializeField]
	string bola_azul;

	[SerializeField]
	string bola_vermelha;

	[SerializeField]
	float fTimeIntervals;

	float fTimer = 0;

	int color = 0;

	public string stringAux;

	void Start () {
		fTimer = fTimeIntervals;
		textDisplay.GetComponent<Text> ().text = "Cor: " + stringAux;
	}

	public void OnCollisionEnter(Collision Collision)
	{ 	
		/// Se tag seja for a desejada, ocorre uma mudança de cena
		if ((Collision.collider.tag == bola_branca && color == 1) || (Collision.collider.tag == bola_roxa && color == 2) || (Collision.collider.tag == bola_preta && color == 3) || (Collision.collider.tag == bola_verde && color == 4) || (Collision.collider.tag == bola_azul && color == 5) || (Collision.collider.tag == bola_vermelha && color == 6)) {
			CorrectMove.score++;
			Debug.Log ("Correto. Pontuacao: " + CorrectMove.score.ToString ());
		} else {
			CorrectMove.score--;
			Debug.Log ("Errado. Pontuacao: " + CorrectMove.score.ToString ());
		}

	}
		
	void Update () {

		fTimer -= Time.deltaTime;

		if (fTimer <= 0) 
		{
			fTimer = fTimeIntervals;

			color = Random.Range(1,7);

			if (color == 1)
				stringAux = "branco";

			if (color == 2)
				stringAux = "roxo";

			if (color == 3)
				stringAux = "preto";

			if (color == 4)
				stringAux = "verde";

			if (color == 5)
				stringAux = "azul";

			if (color == 6)
				stringAux = "vermelho";

			Debug.Log ("Cor: " + stringAux);
			textDisplay.GetComponent<Text> ().text = "Cor: " + stringAux;
		}
	}
}
