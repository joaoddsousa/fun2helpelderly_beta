var searchData=
[
  ['tag_5fright_69',['tag_right',['../class_sub_game1.html#a765200c64f31a51c986238c58b12512b',1,'SubGame1.tag_right()'],['../class_sub_game4.html#a744f10843bacbb3fcbce04ee4e29fe34',1,'SubGame4.tag_right()'],['../class_sub_game5.html#a185a08e4a1f03e50622f5e9fa5d06ab5',1,'SubGame5.tag_right()']]],
  ['tag_5fwrong_70',['tag_wrong',['../class_sub_game1.html#a9a74c1068d8e123a3c531cc3e649a76e',1,'SubGame1.tag_wrong()'],['../class_sub_game4.html#a206587252c7a8d9646b81a2c325a92b2',1,'SubGame4.tag_wrong()'],['../class_sub_game5.html#a7fbddf5fb83676f86537bf7601ec4ab8',1,'SubGame5.tag_wrong()']]],
  ['takingaway_71',['takingAway',['../class_timer_function.html#affb3e3eaebdb8ca4eac2e3f8440acf40',1,'TimerFunction']]],
  ['text_5faux_72',['text_aux',['../class_show_high_score.html#af35890574d537251e889d9842cb3bbe4',1,'ShowHighScore']]],
  ['textdisplay_73',['textDisplay',['../class_score_function.html#afc02183f1c559ba0197f6cc416a944cb',1,'ScoreFunction.textDisplay()'],['../class_sub_game2.html#afbe046b9d01042c549f82a4959fddb2c',1,'SubGame2.textDisplay()'],['../class_timer_function.html#a1851ea2910227c89d99a2475c6ee9186',1,'TimerFunction.textDisplay()']]],
  ['time_74',['time',['../class_serious_game.html#ae3f2f2b6d871519e77bc69857565ea20',1,'SeriousGame']]],
  ['timerfunction_75',['TimerFunction',['../class_timer_function.html',1,'']]],
  ['timerfunction_2ecs_76',['TimerFunction.cs',['../_timer_function_8cs.html',1,'']]],
  ['timertake_77',['TimerTake',['../class_timer_function.html#a65f1e12e903054bffe99e17332f1fdcc',1,'TimerFunction']]],
  ['txtnamesnscore_78',['txtNamesNscore',['../class_show_high_score.html#a8c9771295500a6da79256358d36fdc80',1,'ShowHighScore']]]
];
