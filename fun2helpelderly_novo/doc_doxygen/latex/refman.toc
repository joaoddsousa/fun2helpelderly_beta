\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Hierarchical Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Class Hierarchy}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}Class Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}Class List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}%
\contentsline {chapter}{\numberline {4}Class Documentation}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}Create\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Spawner Class Reference}{7}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Member Function Documentation}{8}{subsection.4.1.1}%
\contentsline {subsubsection}{\numberline {4.1.1.1}Start()}{8}{subsubsection.4.1.1.1}%
\contentsline {subsubsection}{\numberline {4.1.1.2}Update()}{8}{subsubsection.4.1.1.2}%
\contentsline {subsection}{\numberline {4.1.2}Member Data Documentation}{8}{subsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.2.1}decideObject}{8}{subsubsection.4.1.2.1}%
\contentsline {subsubsection}{\numberline {4.1.2.2}fTimeIntervals}{8}{subsubsection.4.1.2.2}%
\contentsline {subsubsection}{\numberline {4.1.2.3}fTimer}{8}{subsubsection.4.1.2.3}%
\contentsline {subsubsection}{\numberline {4.1.2.4}goCreate1}{9}{subsubsection.4.1.2.4}%
\contentsline {subsubsection}{\numberline {4.1.2.5}goCreate2}{9}{subsubsection.4.1.2.5}%
\contentsline {subsubsection}{\numberline {4.1.2.6}goCreate3}{9}{subsubsection.4.1.2.6}%
\contentsline {subsubsection}{\numberline {4.1.2.7}goCreate4}{9}{subsubsection.4.1.2.7}%
\contentsline {subsubsection}{\numberline {4.1.2.8}goCreate5}{9}{subsubsection.4.1.2.8}%
\contentsline {subsubsection}{\numberline {4.1.2.9}goCreateAux}{9}{subsubsection.4.1.2.9}%
\contentsline {subsubsection}{\numberline {4.1.2.10}v3SpawnPosJitter}{10}{subsubsection.4.1.2.10}%
\contentsline {section}{\numberline {4.2}Main\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Menu Class Reference}{10}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{10}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Member Function Documentation}{10}{subsection.4.2.2}%
\contentsline {subsubsection}{\numberline {4.2.2.1}BackToMenu()}{10}{subsubsection.4.2.2.1}%
\contentsline {subsubsection}{\numberline {4.2.2.2}PlayGame()}{11}{subsubsection.4.2.2.2}%
\contentsline {subsubsection}{\numberline {4.2.2.3}QuitGame()}{11}{subsubsection.4.2.2.3}%
\contentsline {section}{\numberline {4.3}Score\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Function Class Reference}{11}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}Member Function Documentation}{11}{subsection.4.3.1}%
\contentsline {subsubsection}{\numberline {4.3.1.1}Start()}{11}{subsubsection.4.3.1.1}%
\contentsline {subsubsection}{\numberline {4.3.1.2}Update()}{12}{subsubsection.4.3.1.2}%
\contentsline {subsection}{\numberline {4.3.2}Member Data Documentation}{12}{subsection.4.3.2}%
\contentsline {subsubsection}{\numberline {4.3.2.1}textDisplay}{12}{subsubsection.4.3.2.1}%
\contentsline {section}{\numberline {4.4}Serious\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game Class Reference}{12}{section.4.4}%
\contentsline {subsection}{\numberline {4.4.1}Member Data Documentation}{12}{subsection.4.4.1}%
\contentsline {subsubsection}{\numberline {4.4.1.1}score}{12}{subsubsection.4.4.1.1}%
\contentsline {subsubsection}{\numberline {4.4.1.2}time}{13}{subsubsection.4.4.1.2}%
\contentsline {section}{\numberline {4.5}Show\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}High\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Score Class Reference}{13}{section.4.5}%
\contentsline {subsection}{\numberline {4.5.1}Member Function Documentation}{13}{subsection.4.5.1}%
\contentsline {subsubsection}{\numberline {4.5.1.1}FillListas()}{14}{subsubsection.4.5.1.1}%
\contentsline {subsubsection}{\numberline {4.5.1.2}OnEnable()}{14}{subsubsection.4.5.1.2}%
\contentsline {subsubsection}{\numberline {4.5.1.3}ReadAndShowScore()}{14}{subsubsection.4.5.1.3}%
\contentsline {subsubsection}{\numberline {4.5.1.4}ShowTopFive()}{14}{subsubsection.4.5.1.4}%
\contentsline {subsection}{\numberline {4.5.2}Member Data Documentation}{14}{subsection.4.5.2}%
\contentsline {subsubsection}{\numberline {4.5.2.1}names}{14}{subsubsection.4.5.2.1}%
\contentsline {subsubsection}{\numberline {4.5.2.2}scores}{14}{subsubsection.4.5.2.2}%
\contentsline {subsubsection}{\numberline {4.5.2.3}text\_aux}{15}{subsubsection.4.5.2.3}%
\contentsline {subsubsection}{\numberline {4.5.2.4}txtNamesNscore}{15}{subsubsection.4.5.2.4}%
\contentsline {section}{\numberline {4.6}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game1 Class Reference}{15}{section.4.6}%
\contentsline {subsection}{\numberline {4.6.1}Member Function Documentation}{16}{subsection.4.6.1}%
\contentsline {subsubsection}{\numberline {4.6.1.1}OnCollisionEnter()}{16}{subsubsection.4.6.1.1}%
\contentsline {subsubsection}{\numberline {4.6.1.2}start()}{16}{subsubsection.4.6.1.2}%
\contentsline {subsubsection}{\numberline {4.6.1.3}WaitForSec\_right()}{16}{subsubsection.4.6.1.3}%
\contentsline {subsubsection}{\numberline {4.6.1.4}WaitForSec\_wrong()}{16}{subsubsection.4.6.1.4}%
\contentsline {subsection}{\numberline {4.6.2}Member Data Documentation}{17}{subsection.4.6.2}%
\contentsline {subsubsection}{\numberline {4.6.2.1}tag\_right}{17}{subsubsection.4.6.2.1}%
\contentsline {subsubsection}{\numberline {4.6.2.2}tag\_wrong}{17}{subsubsection.4.6.2.2}%
\contentsline {subsubsection}{\numberline {4.6.2.3}uiObject\_right}{17}{subsubsection.4.6.2.3}%
\contentsline {subsubsection}{\numberline {4.6.2.4}uiObject\_wrong}{17}{subsubsection.4.6.2.4}%
\contentsline {section}{\numberline {4.7}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game2 Class Reference}{17}{section.4.7}%
\contentsline {subsection}{\numberline {4.7.1}Member Function Documentation}{18}{subsection.4.7.1}%
\contentsline {subsubsection}{\numberline {4.7.1.1}OnCollisionEnter()}{18}{subsubsection.4.7.1.1}%
\contentsline {subsubsection}{\numberline {4.7.1.2}Start()}{19}{subsubsection.4.7.1.2}%
\contentsline {subsubsection}{\numberline {4.7.1.3}Update()}{19}{subsubsection.4.7.1.3}%
\contentsline {subsection}{\numberline {4.7.2}Member Data Documentation}{19}{subsection.4.7.2}%
\contentsline {subsubsection}{\numberline {4.7.2.1}bola\_azul}{19}{subsubsection.4.7.2.1}%
\contentsline {subsubsection}{\numberline {4.7.2.2}bola\_branca}{19}{subsubsection.4.7.2.2}%
\contentsline {subsubsection}{\numberline {4.7.2.3}bola\_preta}{19}{subsubsection.4.7.2.3}%
\contentsline {subsubsection}{\numberline {4.7.2.4}bola\_roxa}{19}{subsubsection.4.7.2.4}%
\contentsline {subsubsection}{\numberline {4.7.2.5}bola\_verde}{20}{subsubsection.4.7.2.5}%
\contentsline {subsubsection}{\numberline {4.7.2.6}bola\_vermelha}{20}{subsubsection.4.7.2.6}%
\contentsline {subsubsection}{\numberline {4.7.2.7}color}{20}{subsubsection.4.7.2.7}%
\contentsline {subsubsection}{\numberline {4.7.2.8}fTimeIntervals}{20}{subsubsection.4.7.2.8}%
\contentsline {subsubsection}{\numberline {4.7.2.9}fTimer}{20}{subsubsection.4.7.2.9}%
\contentsline {subsubsection}{\numberline {4.7.2.10}stringAux}{20}{subsubsection.4.7.2.10}%
\contentsline {subsubsection}{\numberline {4.7.2.11}textDisplay}{21}{subsubsection.4.7.2.11}%
\contentsline {section}{\numberline {4.8}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game3 Class Reference}{21}{section.4.8}%
\contentsline {subsection}{\numberline {4.8.1}Member Function Documentation}{22}{subsection.4.8.1}%
\contentsline {subsubsection}{\numberline {4.8.1.1}OnCollisionEnter()}{22}{subsubsection.4.8.1.1}%
\contentsline {subsubsection}{\numberline {4.8.1.2}Start()}{22}{subsubsection.4.8.1.2}%
\contentsline {subsubsection}{\numberline {4.8.1.3}Update()}{22}{subsubsection.4.8.1.3}%
\contentsline {subsubsection}{\numberline {4.8.1.4}waitfunction()}{22}{subsubsection.4.8.1.4}%
\contentsline {subsection}{\numberline {4.8.2}Member Data Documentation}{22}{subsection.4.8.2}%
\contentsline {subsubsection}{\numberline {4.8.2.1}aux\_score}{22}{subsubsection.4.8.2.1}%
\contentsline {subsubsection}{\numberline {4.8.2.2}strTag}{23}{subsubsection.4.8.2.2}%
\contentsline {subsubsection}{\numberline {4.8.2.3}strTag2}{23}{subsubsection.4.8.2.3}%
\contentsline {subsubsection}{\numberline {4.8.2.4}strTag3}{23}{subsubsection.4.8.2.4}%
\contentsline {subsubsection}{\numberline {4.8.2.5}strTag4}{23}{subsubsection.4.8.2.5}%
\contentsline {subsubsection}{\numberline {4.8.2.6}strTag\_aux}{23}{subsubsection.4.8.2.6}%
\contentsline {section}{\numberline {4.9}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game4 Class Reference}{23}{section.4.9}%
\contentsline {subsection}{\numberline {4.9.1}Member Function Documentation}{24}{subsection.4.9.1}%
\contentsline {subsubsection}{\numberline {4.9.1.1}OnCollisionEnter()}{24}{subsubsection.4.9.1.1}%
\contentsline {subsubsection}{\numberline {4.9.1.2}start()}{25}{subsubsection.4.9.1.2}%
\contentsline {subsubsection}{\numberline {4.9.1.3}Update()}{25}{subsubsection.4.9.1.3}%
\contentsline {subsubsection}{\numberline {4.9.1.4}WaitForSec\_right()}{25}{subsubsection.4.9.1.4}%
\contentsline {subsubsection}{\numberline {4.9.1.5}WaitForSec\_wrong()}{25}{subsubsection.4.9.1.5}%
\contentsline {subsection}{\numberline {4.9.2}Member Data Documentation}{25}{subsection.4.9.2}%
\contentsline {subsubsection}{\numberline {4.9.2.1}fTimer}{25}{subsubsection.4.9.2.1}%
\contentsline {subsubsection}{\numberline {4.9.2.2}nextlvl}{25}{subsubsection.4.9.2.2}%
\contentsline {subsubsection}{\numberline {4.9.2.3}tag\_right}{26}{subsubsection.4.9.2.3}%
\contentsline {subsubsection}{\numberline {4.9.2.4}tag\_wrong}{26}{subsubsection.4.9.2.4}%
\contentsline {subsubsection}{\numberline {4.9.2.5}uiObject\_right}{26}{subsubsection.4.9.2.5}%
\contentsline {subsubsection}{\numberline {4.9.2.6}uiObject\_wrong}{26}{subsubsection.4.9.2.6}%
\contentsline {section}{\numberline {4.10}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game5 Class Reference}{26}{section.4.10}%
\contentsline {subsection}{\numberline {4.10.1}Member Function Documentation}{27}{subsection.4.10.1}%
\contentsline {subsubsection}{\numberline {4.10.1.1}OnCollisionEnter()}{27}{subsubsection.4.10.1.1}%
\contentsline {subsubsection}{\numberline {4.10.1.2}start()}{27}{subsubsection.4.10.1.2}%
\contentsline {subsubsection}{\numberline {4.10.1.3}WaitForSec\_right()}{28}{subsubsection.4.10.1.3}%
\contentsline {subsubsection}{\numberline {4.10.1.4}WaitForSec\_wrong()}{28}{subsubsection.4.10.1.4}%
\contentsline {subsection}{\numberline {4.10.2}Member Data Documentation}{28}{subsection.4.10.2}%
\contentsline {subsubsection}{\numberline {4.10.2.1}tag\_right}{28}{subsubsection.4.10.2.1}%
\contentsline {subsubsection}{\numberline {4.10.2.2}tag\_wrong}{28}{subsubsection.4.10.2.2}%
\contentsline {subsubsection}{\numberline {4.10.2.3}uiObject\_right}{28}{subsubsection.4.10.2.3}%
\contentsline {subsubsection}{\numberline {4.10.2.4}uiObject\_wrong}{28}{subsubsection.4.10.2.4}%
\contentsline {section}{\numberline {4.11}Submit\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Button Class Reference}{29}{section.4.11}%
\contentsline {subsection}{\numberline {4.11.1}Member Function Documentation}{29}{subsection.4.11.1}%
\contentsline {subsubsection}{\numberline {4.11.1.1}CreateText()}{29}{subsubsection.4.11.1.1}%
\contentsline {subsubsection}{\numberline {4.11.1.2}SubmitName()}{29}{subsubsection.4.11.1.2}%
\contentsline {subsection}{\numberline {4.11.2}Member Data Documentation}{30}{subsection.4.11.2}%
\contentsline {subsubsection}{\numberline {4.11.2.1}\_path}{30}{subsubsection.4.11.2.1}%
\contentsline {subsubsection}{\numberline {4.11.2.2}aux\_name}{30}{subsubsection.4.11.2.2}%
\contentsline {subsubsection}{\numberline {4.11.2.3}name}{30}{subsubsection.4.11.2.3}%
\contentsline {section}{\numberline {4.12}Timer\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Function Class Reference}{30}{section.4.12}%
\contentsline {subsection}{\numberline {4.12.1}Member Function Documentation}{31}{subsection.4.12.1}%
\contentsline {subsubsection}{\numberline {4.12.1.1}Start()}{31}{subsubsection.4.12.1.1}%
\contentsline {subsubsection}{\numberline {4.12.1.2}TimerTake()}{31}{subsubsection.4.12.1.2}%
\contentsline {subsubsection}{\numberline {4.12.1.3}Update()}{31}{subsubsection.4.12.1.3}%
\contentsline {subsection}{\numberline {4.12.2}Member Data Documentation}{31}{subsection.4.12.2}%
\contentsline {subsubsection}{\numberline {4.12.2.1}secondsLeft}{31}{subsubsection.4.12.2.1}%
\contentsline {subsubsection}{\numberline {4.12.2.2}takingAway}{32}{subsubsection.4.12.2.2}%
\contentsline {subsubsection}{\numberline {4.12.2.3}textDisplay}{32}{subsubsection.4.12.2.3}%
\contentsline {section}{\numberline {4.13}volume\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Changer\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Script Class Reference}{32}{section.4.13}%
\contentsline {subsection}{\numberline {4.13.1}Member Function Documentation}{32}{subsection.4.13.1}%
\contentsline {subsubsection}{\numberline {4.13.1.1}MyUpdate()}{33}{subsubsection.4.13.1.1}%
\contentsline {subsubsection}{\numberline {4.13.1.2}OnDisable()}{33}{subsubsection.4.13.1.2}%
\contentsline {subsubsection}{\numberline {4.13.1.3}OnEnable()}{33}{subsubsection.4.13.1.3}%
\contentsline {subsection}{\numberline {4.13.2}Member Data Documentation}{33}{subsection.4.13.2}%
\contentsline {subsubsection}{\numberline {4.13.2.1}musicVolumeController}{33}{subsubsection.4.13.2.1}%
\contentsline {subsubsection}{\numberline {4.13.2.2}SliderHandleValue}{33}{subsubsection.4.13.2.2}%
\contentsline {section}{\numberline {4.14}Warmup\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Class Class Reference}{34}{section.4.14}%
\contentsline {subsection}{\numberline {4.14.1}Member Function Documentation}{34}{subsection.4.14.1}%
\contentsline {subsubsection}{\numberline {4.14.1.1}OnCollisionEnter()}{34}{subsubsection.4.14.1.1}%
\contentsline {subsubsection}{\numberline {4.14.1.2}WaitForSec()}{34}{subsubsection.4.14.1.2}%
\contentsline {subsection}{\numberline {4.14.2}Member Data Documentation}{34}{subsection.4.14.2}%
\contentsline {subsubsection}{\numberline {4.14.2.1}strTag}{34}{subsubsection.4.14.2.1}%
\contentsline {chapter}{\numberline {5}File Documentation}{35}{chapter.5}%
\contentsline {section}{\numberline {5.1}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Create\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Spawner.cs File Reference}{35}{section.5.1}%
\contentsline {section}{\numberline {5.2}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Main\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Menu.cs File Reference}{35}{section.5.2}%
\contentsline {section}{\numberline {5.3}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Score\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Function.cs File Reference}{35}{section.5.3}%
\contentsline {section}{\numberline {5.4}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Serious\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game.cs File Reference}{35}{section.5.4}%
\contentsline {section}{\numberline {5.5}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Show\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}High\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Score.cs File Reference}{36}{section.5.5}%
\contentsline {section}{\numberline {5.6}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game1.cs File Reference}{36}{section.5.6}%
\contentsline {section}{\numberline {5.7}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game2.cs File Reference}{36}{section.5.7}%
\contentsline {section}{\numberline {5.8}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game3.cs File Reference}{36}{section.5.8}%
\contentsline {section}{\numberline {5.9}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game4.cs File Reference}{36}{section.5.9}%
\contentsline {section}{\numberline {5.10}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Sub\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Game5.cs File Reference}{36}{section.5.10}%
\contentsline {section}{\numberline {5.11}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Submit\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Button.cs File Reference}{37}{section.5.11}%
\contentsline {section}{\numberline {5.12}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Timer\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Function.cs File Reference}{37}{section.5.12}%
\contentsline {section}{\numberline {5.13}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/volume\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Changer\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Script.cs File Reference}{37}{section.5.13}%
\contentsline {section}{\numberline {5.14}C\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Users/jddso/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}One\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Drive/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Documentos/fun2helpelderly/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Assets/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Scripts/\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Warmup\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}Class.cs File Reference}{37}{section.5.14}%
\contentsline {chapter}{Index}{39}{section*.65}%
